FROM python:3.8
RUN python3.8 -m pip install aiogram pyqiwip2p bs4
WORKDIR /usr/src/app

COPY . .

ENTRYPOINT ["python"]
CMD ["main.py"]
