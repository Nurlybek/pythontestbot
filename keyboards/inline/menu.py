from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from data.config import BTN_REVIEWS_URL, BTN_CHAT_URL, BTN_BOT_RESERV_URL, SUBSCRIPTION_CHANNEL_URL

url_reviews = InlineKeyboardMarkup(resize_keyboard=True)
btn_url_reviews = InlineKeyboardButton(
    text='Отзывы',
    url=BTN_REVIEWS_URL
)
url_reviews.add(btn_url_reviews)

url_chat = InlineKeyboardMarkup(resize_keyboard=True)
btn_url_chat = InlineKeyboardButton(
    text='Чатик',
    url=BTN_CHAT_URL
)
url_chat.add(btn_url_chat)


def get_btn_subscription():
    subscription = InlineKeyboardMarkup(resize_keyboard=True)
    btn_subscription_channael = InlineKeyboardButton(
        text='Подписаться на канал',
        url=SUBSCRIPTION_CHANNEL_URL
    )
    btn_subscription_bot = InlineKeyboardButton(
        text='Подписаться на резервного бота',
        url=BTN_BOT_RESERV_URL
    )

    btn_done = InlineKeyboardButton(
        text='✅ Готово',
        callback_data='done'
    )

    subscription.add(btn_subscription_channael)
    subscription.add(btn_subscription_bot)
    subscription.add(btn_done)

    return subscription
